import React from 'react';
import style from '../../styles/scss/style.scss';
import React_DatePicker from 'react-datepicker';
import moment from 'moment';

class DatePicker extends React.Component {
    static get defaultProps() {
        return {
            startDate: moment()
        };
    }
    constructor(props) {
        super();
    }
    handleChange(date) {
        this.props.onChange(date);
    }

    render() {
        debugger;
        let self = this;
        return <React_DatePicker
        selected={this.props.date}
        locale='en-gb'
        todayButton={'Today'}
        minDate={this.props.startDate.length === 0 ? moment() : this.props.startDate.add(1,'days')}
        placeholderText={this.props.placeholder}
        onChange={this.props.onChange.bind(this)} />;
    }
}

export default DatePicker;
