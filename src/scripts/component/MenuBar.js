import React from 'react';
import style from '../../styles/scss/style.scss';

class MenuBar extends React.Component {
    static get defaultProps() {
        return {
            focused: 0
        };
    }
    constructor(props) {
        super();
    }
    clicked(index) {
        this.props.onMenuBarChange(index);
    }

    render() {
        let self = this;
        return (
            <div className="menuBar-wrapper">
                <ul className="menu-list">{ this.props.items.map(function(m, index){
        
                    var style = '';
                    if(self.props.focused == index){
                        style = 'focused';
                    };
        
                    return <li key={m+index} className={style} onClick={self.clicked.bind(self, index)}>{m}</li>;
        
                }) }
                        
                </ul>
                
            </div>
        );
    }
}

export default MenuBar;
