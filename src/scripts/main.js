import React from 'react';
import ReactDOM from 'react-dom';
import FlightSearchEngine from './module/FlightSearchEngine';

ReactDOM.render(<FlightSearchEngine />, document.getElementById('root'));
