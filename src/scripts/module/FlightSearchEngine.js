import React from 'react';
import style from '../../styles/scss/style.scss';
import FlightSearchFilter from './FlightSearchFilter';
import FlightSearchResult from './FlightSearchResult';

class FlightSearchEngine extends React.Component {
    render() {
        return (
            <div className="container">
            	<header>
            		<h1> Flight Search Engine</h1>
            	</header>
            	<main>
                    <FlightSearchFilter/>
                    <FlightSearchResult/>
            	</main>
        	</div>
        );
    }
}

export default FlightSearchEngine;
