import React from 'react';
import style from '../../styles/scss/style.scss';
import MenuBar from '../component/MenuBar';
import DatePicker from '../component/DatePicker';
import moment from 'moment';


class FlightSearchFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            focused: 0,
            origin: "",
            destination: "",
            startDate: "",
            returnDate: "",
            personCount: "1",

        };
    }
    onOriginChange(event) {
        this.setState({ origin: event.target.value });
    }
    onDestinationChange(event) {
        this.setState({ destination: event.target.value });
    }

    handleCountChange(event) {
        this.setState({ personCount: event.target.value });
    }
    onMenuBarChange(index) {
        this.setState({
            focused: index
        });
    }
    handleStartDateChange(date) {
        this.setState({
            startDate: date
        });
    }
    handleReturnDateChange(date) {
        this.setState({
            returnDate: date
        });
    }

    handleFormSubmit(event) {
        event.preventDefault();
        console.log('startDate', this.state.startDate);
        console.log('startDate', this.state.returnDate);
    }
    render() {
        return (
            <div className="filter-container">

                <MenuBar items={['One Way', 'Return']}
                onMenuBarChange={this.onMenuBarChange.bind(this)}
                focused = {this.state.focused}
            	/>

				<div className="form-wrapper">
	                <form onSubmit={this.handleFormSubmit.bind(this)}>
		                <input type="text" value={this.state.origin} onChange={this.onOriginChange.bind(this)} placeholder="Enter Origin City"/>
		                <input type="text" value={this.state.destination} onChange={this.onDestinationChange.bind(this)} placeholder="Enter Destination City"/>

		                <DatePicker 
			                placeholder = "Departure Date" 
			                date={this.state.startDate} 
			                onChange={this.handleStartDateChange.bind(this)}
			            />

		                {this.state.focused===1 ? 
		                	<DatePicker 
		                	placeholder = "Return Date" 
		                	startDate={this.state.startDate} 
		                	date={this.state.returnDate} 
		                	onChange={this.handleReturnDateChange.bind(this)}/> : null}

		                <select value={this.state.personCount} className="select-control" onChange={this.handleCountChange.bind(this)}>
						    <option value="1">1</option>
						    <option value="2">2</option>
						    <option value="3">3</option>
						    <option value="4">4</option>
						    <option value="5">5</option>
						 </select>
		                <input className="button" type="submit" value="Submit"/>
	                </form>
                </div>
        	</div>
        );
    }
}

export default FlightSearchFilter;
